package no.ntnu.imt3281.eksamen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

/**
 * Utgangspunkt for swapi-import oppgaven
 */
public class App {
    public static void main(String[] args) {
        String dbURL = "jdbc:derby:../swapiDB";
        Connection con = null;
        try {                                                   // Attempt to connect to DB
            con = DriverManager.getConnection(dbURL);
        } catch (SQLException e) {                              // if it failed
            if (e.getMessage().equals("Database '../swapiDB' not found.")) {    // Is the database missing
                try {                                           // Attempt to create the DB
                    con = DriverManager.getConnection(dbURL+";create=true");
                    // TODO: Need to create table
                } catch (SQLException e1) {                     // If creation failed, exit
                    System.err.println("Kunne ikke opprette databasen");
                    System.exit(1);
                }
            } else {                                            // Database exists, but could not connect. Exit.
                System.err.println("Kunne ikke koble til databasen");
                System.exit(1);
            }
        }

        URL url = null;
        try {
            url = new URL("https://swapi.co/api/films/1/");           // Get information about film #1
            URLConnection connection = url.openConnection();                // Need a bit extra
            // Must set user-agent, the java default user agent is denied
            connection.setRequestProperty("User-Agent", "curl/7.8 (i386-redhat-linux-gnu) libcurl 7.8 (OpenSSL 0.9.6b) (ipv6 enabled)");
            // Must set accept to application/json, if not html is returned
            connection.setRequestProperty("Accept", "application/json");
            connection.connect();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String tmp;
            while ((tmp = br.readLine())!=null) {                           // Dump contents to console
                System.out.println(tmp);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
