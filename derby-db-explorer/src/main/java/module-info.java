module no.ntnu.imt3281.eksamen {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens no.ntnu.imt3281.eksamen to javafx.fxml;
    exports no.ntnu.imt3281.eksamen;
}