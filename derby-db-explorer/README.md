# JavaFX og nyere utgaver av Java

Dette prosjektet er generert i følge openjfx sin standard, prosjektet bruker moduler, så se på "Modular with Maven" på de to linkene under.

Følg denne for å kompilere/kjøre i IntelliJ : (https://openjfx.io/openjfx-docs/#IDE-Intellij)

Dersom du bruker Eclipse så finner du informasjon på : (https://openjfx.io/openjfx-docs/#IDE-Eclipse)

