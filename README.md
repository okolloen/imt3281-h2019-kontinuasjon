# Oppgave 1 (teller 40%)

https://swapi.co inneholder informasjon om filmer, karakterer, planeter osv fra Star Wars universet. [Full dokumentasjon](https://swapi.co/documentation#intro) er tilgjengelig online.

I katalogen *swapi-import* i dette repositoriet finner du et enkelt program som kobler til en derby database og henter informasjon om den første filmen fra *swapi.co*.

Skriv koden for å opprette tabell(er) som kan inneholde all informasjonen fra *swapi.co* og koden som skal til for å hente all informasjon fra *swapi.co*. Du skal altså lage kode som kan lage en lokal kopi av databasen hos *swapi.co*.

Programmet ditt skal benytte fem (5) tråder for å lese informasjon fra *swapi.co*, bruk gjerne en LinkedBlockingQueue for å hjelpe til med synkroniseringen av tråder. Ingen informasjon skal leses mer enn en gang og programmet skal avslutte når all informasjon er lest og lagret i den lokale databasen.

Når programmet avsluttes skal det skrives en oversikt til konsollet over hvor mange elementer som er funnet i hver enkelt kategori slik som vist under.

![Illustrasjonsbilde](https://bitbucket.org/okolloen/imt3281-h2019-kontinuasjon/downloads/Skjermbilde%202019-07-29%20kl.%2009.02.25.png)

# Oppgave 2 (teller 20%)

Dersom du ikke får til oppgave 1 så legger du inn en fil "GirOppOppgave1.txt" i rotkatalogen i repositoriet (samme sted som denne README.md filen) før du sender meg en e-post og ber om å få databasen. Jeg vil da sende en kopi av databasen fra løsningsforslaget sammen med den koden som er brukt for å opprette denne slik at du kan gjør oppgave 2.

[Spark](http://sparkjava.com/) er et rammeverk for å forenkle det å lage HTTP servere (web server) i Java. I katalogen *swapi-server* ligger et prosjekt som viser en enkel server som ligner på "swapi.co". Med utgangspunkt i koden i *swapi-server* skal du lage kode som gjør at denne fungerer på samme måte som *swapi.co". Dvs at når en bruker adressen *http://localhost:4567/films/1* så skal serveren svare med JSON dataene for denne filmen. Når en bruker adressen *http://localhost:4567/planets/1* så skal serveren svare med JSON dataene for denne planeten osv for alle kategorier (filmer, planeter, karakterer osv).

Serveren trenger ikke gi noe output på servere siden, den skal kun svare på HTTP forespørsler.

# Oppgave 3 (teller 40%)

Katalogen *derby-db-explorer* inneholder alt som skal til for å lage en JavaFX applikasjon, se [readme](derby-db-explorer/README.md) i den katalogen for hvordan en nå (Java >9) kompilerer og kjører en JavaFX applikasjon.

I svært mange prosjekter benyttes *Apache Derby* som database, det er ønskelig med en applikasjon som gjør det enkelt å se og manipulere data i en slik database. Du skal nå lage en slik applikasjon. Denne applikasjonen skal se ut og fungere slik som vist under.

![Illustrasjonsbilde](https://bitbucket.org/okolloen/imt3281-h2019-kontinuasjon/downloads/derbyExplorer.gif)

Til venstre i applikasjonen er en knapp som benyttes når en ønsker å åpne en database, benytt JavaFX sin DirectoryChooser component for å la brukeren velge hvilken katalog som inneholder Derby databasen som skal åpnes. Rett under denne knappen er en label hvor du kan vise hvilken database som eventuelt er åpen.

Videre er en liste med alle tabeller i den åpne databasen. Når en bruker velger en tabell i denne listen skal automatisk alt innholdet fra denne tabellen vises i tabellvisningen til høyre. Denne tabellen må altså tilpasses etter innholdet i valgt tabell, slik at riktig antall kolonner og riktige kolonnetitler vises avhengig av innholdet i tabellen som er valgt.

Videre nedover til venstre er et felt som viser hva som er valgt, dvs at når en bruker velger en tabell i listen så er det "SELECT * FROM 'tabellnavn'", brukeren skal her selv kunne skrive inn egen SQL som kjøres når vedkommende trykker på "Execute query" knappen. Tabellvisningen til høyre skal da oppdateres.

Når brukeren høyreklikker (velger context meny) i tabellen til høyre skal det komme frem en context meny som lar brukeren enten slette gjeldende rad eller redigere innholdet i denne raden. Om brukerne velger å slette (Delete) så skal raden fjernes fra tabellen. Dersom brukeren velger å redigere (Edit) så skal det dukke opp en dialog med alle felter fra denne raden slik at brukeren kan redigere disse (se illustrasjonen over). 

Dersom brukeren skriver inn et uttrykk for å endre, slette eller legge til data i feltet som viser SQL spørringen og trykker på knappen "Execute" så skal data i databasen oppdateres før innholdet i tabellen til høyre vises på nytt med disse endringene.

NB! Når brukeren trykker på en av kolonneoverskriftene så vil innholdet i tabellen sorteres på denne kolonnen, dette bør du ta høyde for når en bruker høyreklikker og velger å redigere en rad i tabellen slik at ikke resultatet lagres på feil sted i databasen.

Sørg også for at når brukeren reskalerer en kolonne (endrer bredden på den) så skal fortsatt tabellen bruke hele tilgjengelige bredde (men ikke mer, dvs ingen horisontal skrolling). Se mot slutten av illustrasjonen over.
