package no.ntnu.imt3281.eksamen;

import static spark.Spark.*;

/**
 * Kode som brukes om utgangspunkt for oppgave 2, swapi server
 */
public class App {

    public static void main(String[] args) {
        get("/:type/:nr", (req, res) -> {               // I nettleseren, skriv inn "localhost:4567/people/32
            res.type("application/json");
            return req.params("type") + ":" + req.params("nr");   // req.params henter navngitte
        });
    }
}
